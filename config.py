import os
import torch
from torchviz import make_dot
from data.labels import LABELS

class Config:

	def __init__(self, net_name, ds_base_dir=os.path.join(os.path.dirname(__file__), 'data')):
		self.NET_NAME = net_name
		self.GPU_ID = 0
		self.NUM_CLASSES = 6
		self.DS_BASE_DIR = ds_base_dir
		self.IMG_DIR = 'images'
		self.MASK_DIR = 'masks'
		self.YOLO_IMG_DIR = 'yolo_images'
		self.YOLO_LABEL_DIR = 'yolo_ann_csv'
		self.BATCH_SIZE = 8
		self.NUM_INPUT_CHANNELS = 3
		self.BACKBONE_WNAME = None
		self.INITIAL_WNAME = '%s_model_best' % net_name
		self.SAVE_WNAME = net_name + '_model_best_%Y-%m-%d_%H:%M:%S'
		self.LEARNING_RATE = 0.02
		self.NUM_EPOCHS = 200

		self.MONTAGE_DIR = os.path.join(self.DS_BASE_DIR, 'test_images')
		self.LOG_DIR = os.path.join(self.DS_BASE_DIR, '%s_logs' % net_name)
		# dir with images to predict
		self.PREDICT_INPUT_DIR = os.path.join(self.DS_BASE_DIR, 'input')
		# dir to output predicted images
		self.PREDICT_OUTPUT_DIR = os.path.join(self.DS_BASE_DIR, 'output')
		# dir with video to predict
		self.PREDICT_VIDS_INPUT_DIR = os.path.join(self.DS_BASE_DIR, 'input_vids')
		# dir to output predicted videos
		self.PREDICT_VIDS_OUTPUT_DIR = os.path.join(self.DS_BASE_DIR, 'output_vids')

	def is_cuda(self):
		return self.GPU_ID >= 0

	def device(self):
		return torch.device("cuda" if torch.cuda.is_available() else "cpu")

	def net_png(self, model_outputs):
		make_dot(model_outputs).render(os.path.join('graphs', self.NET_NAME))

	def apply_labels(self, LABELS, val_field):
		label_map = {}
		for l in LABELS:
			val = l[val_field] if val_field in l else l['val']
			if val:
				label_map[l['val']] = val
			# +1 for background
		self.NUM_CLASSES = max(label_map.values()) + 1
		self.LABEL_MAP = label_map
		return self.LABEL_MAP

	def val_label_map(self):
		self.VAL_LABEL_MAP = {}
		for l in LABELS:
			if l['segnet_val'] not in self.VAL_LABEL_MAP:
				for sl in LABELS:
					if sl['val'] == l['segnet_val']:
						self.VAL_LABEL_MAP[l['segnet_val']] = sl.get('segnet_class', l['class'])
						break
		return self.VAL_LABEL_MAP

