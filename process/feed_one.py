#!/usr/bin/env python

import redis, os, sys, json

r = redis.Redis()

ifname = sys.argv[1] if len(sys.argv) > 2 else 'http://localhost/~n800s/test.mp4'

#r.lpush('l.uni_request', json.dumps({'uri': os.path.abspath(sys.argv[1])}))
#r.lpush('l.uni_request', """
#{
#    "algorithm": "default",
#    "media": 504179,
#    "counter": 222,
#    "transponder": 30,
#    "uri": "http://172.30.0.117/media/upload/EE02000000000000104208008000_1596715345.38.mp4",
#    "ttype": "belaz",
#    "arch": 1984799
#}
#""")
#r.lpush('l.uni_request', json.dumps({
#		"arch": 2080056,
#		"uri": "http://172.30.0.117/media/upload/57002010012A400828091820_1602555727.72_19.mp4",
#		"ttype": "belaz",
#		"transponder": 28,
#		"get_debug": true,
#		"counter": 222,
#		"retrain": true,
#		"media": 531029,
#	}))

r.lpush('l.uni_request', json.dumps({
    "get_debug": True,
    "algorithm": "segnet_only",
    "get_files": [
        "*.json",
        "*output*.avi"
    ],
    "media_uri": ifname,
    "obj_type": "truck",
    "callback_uri": "file:///tmp/",
    "req_debug": True
}))

