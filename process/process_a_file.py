import sys, os, redis, json

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

process_file = None

idata = json.loads(sys.stdin.read())
if idata['algorithm'] == 'segnet_only':

	from predict_segnet_video import process_file

if process_file:
	odata = process_file(*idata['args'], **idata['kwds'])
	if odata:
		r = redis.Redis()
		r.set(idata['reply'], json.dumps(odata))
	else:
		print('File processing returned no data')
		sys.exit(1)
else:
	print(f'Algorithm {idata["algorithm"]} is unknown', file=sys.stderr)
	sys.exit(2)

