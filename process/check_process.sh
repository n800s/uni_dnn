#!/bin/sh

cd `dirname $0`

for pname in uni_dnn_process_t uni_dnn_tserver
do


if ps -eo pid,command |grep python|grep -E "$pname"|grep -v grep;
then
		echo "$(date '+%Y %b %d %H:%M')|| $pname is still running." >> check_process.log
else
		echo "$(date '+%Y %b %d %H:%M')|| $pname is stopped." >> check_process.log
		./$pname.sh
		echo "$(date '+%Y %b %d %H:%M')|| $pname was started." >> check_process.log
fi

done
