#!/bin/bash

export PATH=${PATH}:~/.local/bin:/opt/cuda/bin:~/bin
export LD_LIBRARY_PATH=${HOME}/.local/lib
export CUDA_VISIBLE_DEVICES=0
export NVIDIA_VISIBLE_DEVICES=0

cd `dirname $0`
python3 -u ./uni_dnn_process_t.py &> uni_dnn_process_t.log &
echo $?
