#!/usr/bin/env python3

import redis, time, json, sys, os, traceback, tempfile, shutil, glob

import argparse
import wget
import subprocess
from multiprocessing import Process, Queue
from itertools import chain

def dbprint(text):
	print(time.strftime('%Y-%m-%d %H:%M:%S'), text, file=sys.stdout)
	sys.stdout.flush()

TMP_PATH = os.path.expanduser('~/tmp/uni_dnn')
if not os.path.exists(TMP_PATH):
	os.makedirs(TMP_PATH)

ap = argparse.ArgumentParser()
ap.add_argument('--redis_host', type=str, default='localhost')
args = vars(ap.parse_args())

r = redis.Redis(host=args['redis_host'])

dbprint('Started')

def uploader(q):
	while True:
		try:
			item = q.get()
#			dbprint('item:{}'.format(item))
			if item.get('quit', False):
				break
			dbprint('Executing: %s' % ' '.join(item['cmd']))
			subprocess.check_output(item['cmd'])
			for fname in item.get('filelist', []):
				os.unlink(fname)
			if 'tmpdir' in item:
				shutil.rmtree(item['tmpdir'])
		except Exception as e:
			dbprint(e)

q = Queue()
p = Process(target=uploader, args=(q,))
p.start()

vext = '.webm'
codec = 'VP80'

while True:

	try:
		req = r.brpop('l.uni_request', 0)[1].decode("utf-8")
		req = json.loads(req)
		try:
			debug = os.path.exists(os.path.join(os.path.dirname(__file__), 'debug'))
			if debug:
				dbprint('request: %s' % json.dumps(req, indent=4))
			req_debug = req.get('get_debug', False)
			tmpdir = tempfile.mkdtemp(prefix='wget_', dir=TMP_PATH)
			media_obj = req.get('media_obj', None)
			if media_obj is None:
				media_uri = req.get('media_uri', None)
				if not media_uri:
					media_uri = req.get('uri', None)
				dbprint('Reading ... %s' % media_uri)
				fname = wget.download(media_uri, out=tmpdir, bar=None)
			else:
				dbprint('req media obj size: %s' % len(media_obj))
				f = tempfile.NamedTemporaryFile(dir=tmpdir)
				f.write(media_obj)
				f.flush()
				fname = f.name
			dbprint('Processing %s' % fname)
			bname = os.path.splitext(fname)[0]
			ofname = None
			ofname_montage = None
			if debug or req_debug:
				ofname = bname + '.output' + vext
			if debug or req_debug:
				ofname_montage = bname + '.montage' + vext
			idata = {
				'args': [],
				'kwds': {
					'codec': codec,
					'ifname': fname,
					'ofname': ofname,
					'ofname_montage': ofname_montage,
				},
				'obj_type': req.get('obj_type', None),
				'algorithm': req.get('algorithm', 'default'),
				'reply': 'process_t_reply',
			}
			handler = subprocess.Popen([sys.executable, 'process_a_file.py'], stdin=subprocess.PIPE, stderr=subprocess.PIPE)
			sout,serr = handler.communicate(json.dumps(idata).encode())
			if handler.returncode == 0:
				data = json.loads(r.get(idata['reply']))
			else:
				data = None
				print('Status code:', handler.returncode, file=sys.stderr)
				print(serr.decode(), file=sys.stderr)
			if data:

				callback_files = []
				callback_uri = req.get('callback_uri', None)
				json_fname = fname + '.json'
				json.dump(data, open(json_fname, 'w'), indent=4)
				get_files = [os.path.basename(gn) for gn in req.get('get_files', ['*'])]
				callback_files = list(set(chain(*[glob.glob(os.path.join(tmpdir, gn)) for gn in get_files])))
				cmd = ['curl']
				total_size = 0
				for fname in callback_files:
					cmd +=  ['-F', 'file[]=@{};filename={}'.format(fname, os.path.basename(fname))]
					total_size += os.path.getsize(fname)
				cmd.append(callback_uri)
				dbprint("Total upload file size: {}".format(total_size))
				item = {'cmd': cmd}
				if not debug:
					item['filelist'] = callback_files
					item['tmpdir'] = tmpdir
				q.put(item)
		except KeyboardInterrupt:
			raise
		except:
			req['error'] = traceback.format_exc()
			dbprint(req['error'])
			r.lpush('l.uni_errors', json.dumps(req))
	except KeyboardInterrupt:
		q.put({'quit': True})
		p.join()
		raise
	except:
		pass
