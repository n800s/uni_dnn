import os
import sys
import glob
import json
from datasets import SegVideoDataset
from models import SegNet
from data.labels import LABELS
from config import Config

C = Config('segnet')
C.BATCH_SIZE = 4
C.PREDICT_VIDS_OUTPUT_DIR += '.segnet'

import cv2
import torch
from utils.misc import create_or_clean_dirs
from utils.inference import predict_video_dir, predict_video_file

def load_model():
	model = SegNet(input_channels=C.NUM_INPUT_CHANNELS, output_channels=C.NUM_CLASSES).cuda(C.GPU_ID)
	model.load_state_dict(torch.load(os.path.join(C.DS_BASE_DIR, C.INITIAL_WNAME) + '.pth'))
	return model

def process_file(ifname, ofname, ofname_montage, codec = 'XVID'):
	C.apply_labels(LABELS, 'segnet_val')
	return predict_video_file(C, SegVideoDataset, load_model(), ifname, ofname, ofname_montage, codec=codec)

if __name__ == "__main__":

	C.apply_labels(LABELS, 'segnet_val')
	predict_video_dir(C, SegVideoDataset, load_model())
	print('Finished')
