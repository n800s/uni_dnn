#!/usr/bin/env python3

import os
import glob
import time
import json
import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from datasets import SegTrainDataset
from models import SegNet
from utils.misc import save_model_state, make_test_masked_images, get_criterion, create_or_clean_dirs, profile_model
import numpy as np
from data.labels import LABELS
from config import Config

C = Config('segnet')
C.NUM_EPOCHS = 300

def train(train_dataset):

	train_dataloader = DataLoader(train_dataset, batch_size=C.BATCH_SIZE, shuffle=True, num_workers=0, pin_memory=True)

	model = SegNet(input_channels=C.NUM_INPUT_CHANNELS, output_channels=C.NUM_CLASSES)
	if C.is_cuda():
		model = model.cuda(C.GPU_ID)

	profile_model(model)

	criterion = get_criterion(train_dataset, gpu_id=C.GPU_ID)

	initial_wname = os.path.join(C.DS_BASE_DIR, C.INITIAL_WNAME) + '.pth'
	if os.path.exists(initial_wname):
		print("Load pretrained weights from {}".format(initial_wname))
		model.load_state_dict(torch.load(initial_wname))

	optimizer = torch.optim.Adam(model.parameters(), lr=C.LEARNING_RATE)

	with SummaryWriter(C.LOG_DIR, comment=f'LR_{C.LEARNING_RATE}_BS_{C.BATCH_SIZE}') as tf_writer:
		is_better = True
		prev_loss = float('inf')

		model.train()

		global_step = 0
		for epoch in range(C.NUM_EPOCHS):
			loss_f = 0
			t_start = time.time()

			for batch in train_dataloader:
#				print('@@@@@@@@@@@', batch['image'][0].shape, batch['mask'][0].shape)
				input_tensor = torch.autograd.Variable(batch['image'])
				target_tensor = torch.autograd.Variable(batch['mask'])

				# make sample and save
#				print('!!!!!!!!!!!', batch['fname'])
				if C.MONTAGE_DIR and epoch == 0:
					make_test_masked_images(C.MONTAGE_DIR, batch)

#				print(batch['fname'])
#				print('input tensor', input_tensor.shape)
#				print('target tensor min/max', target_tensor.shape, target_tensor.min(), target_tensor.max())

				if C.is_cuda():
					input_tensor = input_tensor.cuda(C.GPU_ID)
					target_tensor = target_tensor.cuda(C.GPU_ID)


				outputs = model(input_tensor)

				if global_step == 0:
					C.net_png(outputs)
				predicted_tensor, softmaxed_tensor = outputs

				optimizer.zero_grad()
				loss = criterion(predicted_tensor, target_tensor)
				tf_writer.add_scalar('Loss/train', loss.item(), global_step)
#				tf_writer.add_scalar('prediction/train', softmaxed_tensor, global_step)
				loss.backward()
				optimizer.step()


				loss_f += loss.float()

			prediction_f = softmaxed_tensor.float()
#			print('prediction:', prediction_f)

			delta = time.time() - t_start
			is_better = loss_f < prev_loss

			print("Epoch #{}\tLr: {:g}\tLoss: {:.8f}\t Time: {:2f}s".format(epoch+1, optimizer.param_groups[0]['lr'], loss_f, delta), end='')
			if is_better:
				print(' {:.3f} better'.format(prev_loss-loss_f))
				prev_loss = loss_f
				save_model_state(model, C.DS_BASE_DIR, '%s_%.4f' % (C.SAVE_WNAME, loss_f), C.INITIAL_WNAME)

				if epoch % 5 == 1:
					for param_group in optimizer.param_groups:
						param_group['lr'] *= 0.90
			else:
				print(' no better')

			global_step += 1
			if global_step % (len(train_dataset) // (10 * C.BATCH_SIZE)) == 0:
				for tag, value in model.named_parameters():
					tag = tag.replace('.', '/')
					print('tag=', tag)
					tf_writer.add_histogram('weights/' + tag, value.data.cpu().numpy(), global_step)
#					tf_writer.add_histogram('grads/' + tag, value.grad.data.cpu().numpy(), global_step)
#				tf_writer.add_image('prediction', prediction_f, global_step)
				tf_writer.add_scalar('learning_rate', optimizer.param_groups[0]['lr'], global_step)

if __name__ == "__main__":

	create_or_clean_dirs((C.MONTAGE_DIR, C.LOG_DIR))
	label_map = C.apply_labels(LABELS, 'segnet_val')
	print('label_map=', C.LABEL_MAP, C.NUM_CLASSES)
	ds = SegTrainDataset(img_dir=os.path.join(C.DS_BASE_DIR, C.IMG_DIR), mask_dir=os.path.join(C.DS_BASE_DIR, C.MASK_DIR),
		num_classes=C.NUM_CLASSES, img_size=(224, 224), label_map=label_map)
	train(ds)
	print('Finished')
