import os
import glob
import torch
import time
import cv2
from PIL import Image
import numpy as np
import torch.autograd.profiler as profiler
from .colors import label_preview_colors
from .resultsmontage import ResultsMontage

def save_model_state(model, dname, wbname_tpl, symlink_bname):
	slink_name = os.path.join(dname, symlink_bname + '.pth')
	wname = time.strftime(wbname_tpl) + '.pth'
	torch.save(model.state_dict(), os.path.join(dname, wname))
	print("Model checkpoint saved to %s in %s" % (wname, dname))
	if os.path.exists(slink_name) or os.path.islink(slink_name):
#		print('file or link exists, unlink')
		os.unlink(slink_name)
	os.symlink(wname, slink_name)

def make_test_masked_images(dest_dir, batch):
	m2colour = np.vectorize(lambda v: np.array(label_preview_colors[v]))
	for i in range(len(batch['fname'])):
		test_fname = os.path.join(dest_dir, os.path.basename(batch['fname'][i]))
		if not os.path.exists(test_fname):
			montage = ResultsMontage(batch['mask'][i].shape, 2, 2)
#			img = (np.transpose(batch['image'][i], (2,1,0))*255).data.numpy().astype(np.uint8)
			img = (np.transpose(batch['image'][i].data.numpy(), (1,2,0))*255).astype(np.uint8)
			mask = batch['mask'][i].data.numpy().astype(np.uint8)
			montage.addResult(img)
			m = np.zeros(img.shape, img.dtype)
#			print('mask shape', mask.shape)
#			print('m shape', m.shape)
			num_classes = mask.max()
			with np.nditer(mask, op_flags=['readwrite'], flags=['multi_index']) as it:
				for item in it:
					clr = label_preview_colors[item][:3]
#					m[it.multi_index[0], it.multi_index[1]] = clr
					m[it.multi_index[0], it.multi_index[1]] = clr[0]
#					print('clr , mindex', clr, it.multi_index)
			montage.addResult(m)
			im = Image.fromarray(montage.montage)
			im.save(test_fname, 'PNG')

def get_criterion(train_dataset, gpu_id=-1):
	class_weights = train_dataset.get_class_probability()
	if gpu_id >= 0:
		class_weights = class_weights.cuda(gpu_id)

#	class_weights = 1 / class_weights
	print('class_weights', class_weights)

	criterion = torch.nn.CrossEntropyLoss(weight=class_weights)
	if gpu_id >= 0:
		criterion = criterion.cuda(gpu_id)
	return criterion

def create_or_clean_dirs(dirlist, extensions=('.png', '.mp4', '*.json')):
	for dname in dirlist:
		if dname:
			if os.path.exists(dname):
				for root, dirs, files in os.walk(dname, topdown=False):
					for name in files:
						if os.path.splitext(name)[-1] in extensions:
							os.unlink(os.path.join(root, name))
					for name in dirs:
						name = os.path.join(root, name)
						if not os.listdir(name):
							os.rmdir(name)
			else:
				os.makedirs(dname)

def profile_model(m):
	x = torch.randn((1, 3, 224, 224), requires_grad=True).cuda()
	with torch.autograd.profiler.profile(use_cuda=True, record_shapes=True) as prof:
		m(x)
	print(prof.key_averages(group_by_input_shape=True).table(sort_by="cpu_time_total", row_limit=10))
#	print(prof.key_averages().table(sort_by="self_cpu_time_total"))
