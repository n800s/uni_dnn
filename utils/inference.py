import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.switch_backend('agg')
plt.axis('off')
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

import os
import io
import json
import torch
import cv2
import numpy as np
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from .misc import create_or_clean_dirs

def predict_normalised_batch(model, batch, gpu_id=-1):
	input_tensor = torch.autograd.Variable(batch['image'])

#	print('before is_cuda:', input_tensor.is_cuda)

	if gpu_id >= 0:
		input_tensor = input_tensor.cuda(gpu_id)

#	print('after is_cuda:', input_tensor.is_cuda)

	predicted_tensor, softmaxed_tensor = model(input_tensor)

	return input_tensor, predicted_tensor, softmaxed_tensor

def predict_segmentation(model, input_dataset, batch_size, gpu_id=-1, video_mode=False):

	input_dataloader = DataLoader(input_dataset, batch_size=batch_size, shuffle=False, num_workers=0 if video_mode else 4)

	model.eval()

	for batch_idx,batch in enumerate(input_dataloader):

		input_tensor, predicted_tensor, softmaxed_tensor = predict_normalised_batch(model, batch, gpu_id)

		for idx, predicted_mask in enumerate(softmaxed_tensor):

			fig = plt.figure(dpi=180)

			a = fig.add_subplot(1,2,1)
			input_image = input_tensor[idx].cpu().transpose(0, 2)
			plt.imshow(input_image)
			a.set_title('Input Image')

			a = fig.add_subplot(1,3,2)
			predicted_mx = predicted_mask.detach().cpu().numpy()
			predicted_mx = predicted_mx.argmax(axis=0).transpose(1,0)

			found_classes,pixel_counts = np.unique(predicted_mx, return_counts=True)
			frame_stat = dict(zip([round(v, 2) for v in found_classes], [round(v*100/predicted_mx.size, 2) for v in pixel_counts]))

			plt.imshow(predicted_mx)
			a.set_title('Predicted Mask')

			bname = os.path.splitext(os.path.basename(batch['fname'][idx]))[0]

#			fig.savefig(os.path.join(output_dir, "{}_{}.png".format(bname, idx)))

			fig.canvas.draw()
			ncols, nrows = fig.canvas.get_width_height()
			img_arr = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='').reshape(nrows, ncols, 3)

#			buf = io.BytesIO()
#			fig.savefig(buf, format="png", dpi=180)
#			buf.seek(0)
#			img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
#			img_arr = cv2.imdecode(img_arr, 1)
#			buf.close()
			plt.close(fig)

			yield bname, idx, img_arr, frame_stat

def predict_video_file(C, input_dataset_class, model, fname, ofname, ofname_montage, codec='XVID'):
	C.val_label_map()
	input_dataset = input_dataset_class(vid_path=fname, frame_size=(224, 224))
	out = None
	stat_data = []
	for bname,idx,img_arr,frame_stat in predict_segmentation(model, input_dataset, batch_size=C.BATCH_SIZE, gpu_id=C.GPU_ID, video_mode=True):
		if out is None:
			if not os.path.exists(os.path.dirname(ofname)):
				os.makedirs(os.path.dirname(ofname))
#			out = cv2.VideoWriter(ofname, cv2.CAP_FFMPEG, fourcc=cv2.VideoWriter_fourcc(*codec), fps=input_dataset.fps, frameSize=tuple(reversed(img_arr.shape[:2])))
			out = cv2.VideoWriter(ofname, cv2.VideoWriter_fourcc(*codec), input_dataset.fps, tuple(reversed(img_arr.shape[:2])))
#		print('write', idx)
		out.write(img_arr)
#		print(frame_stat)
		label_stat = {}
		for k,v in frame_stat.items():
			if k in C.VAL_LABEL_MAP:
				label_stat[C.VAL_LABEL_MAP[k]] = v
		stat_data.append(label_stat)
	if not out is None:
		out.release()

def predict_video_dir(C, input_dataset_class, model):
	create_or_clean_dirs((C.PREDICT_VIDS_OUTPUT_DIR, ))
	print('Input dir:', C.PREDICT_VIDS_INPUT_DIR)
	for root, dirs, files in os.walk(C.PREDICT_VIDS_INPUT_DIR, topdown=False):
		for bfname in files:
			if os.path.splitext(bfname)[-1] == '.mp4':
				fname = os.path.join(root, bfname)
				print(fname)
				ofname = os.path.join(C.PREDICT_VIDS_OUTPUT_DIR, bfname)
				ofname_montage = os.path.join(C.PREDICT_VIDS_OUTPUT_DIR, os.path.splitext(bfname)[0] + '_montage.mp4')
				stat_data = predict_video_file(C, input_dataset_class, model, fname, ofname, ofname_montage, codec='mp4v')
				json.dump(stat_data, open(ofname + '.json', 'wt'), indent=2)
