#!/usr/bin/env python3

import os
import glob
import time
import json
import torch
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import torch.optim as optim
import torch.nn as nn
from torchsummary import summary
from datasets import YoloTrainDataset
from models import Darknet
from utils.misc import save_model_state, make_test_masked_images, get_criterion, create_or_clean_dirs
import numpy as np
from config import Config
from data.yolo3_params import TRAINING_PARAMS

C = Config('yolo_v3')
C.BACKBONE_WNAME = 'weights/darknet53_weights_pytorch.pth'
C.BATCH_SIZE = 16
C.NUM_EPOCHS = 200

def _save_checkpoint(model, loss_f, epoch):
	save_model_state(model, C.DS_BASE_DIR, '%s_%.4f_ep%s' % (C.SAVE_WNAME, loss_f, epoch), C.INITIAL_WNAME)

if __name__ == "__main__":

	create_or_clean_dirs((C.MONTAGE_DIR, C.LOG_DIR))
#	print(TRAINING_PARAMS)

	dataset = YoloTrainDataset(os.path.join(C.DS_BASE_DIR, C.YOLO_IMG_DIR), os.path.join(C.DS_BASE_DIR, C.YOLO_LABEL_DIR), img_size=(224, 224))
	json.dump(dataset.labels(), open(os.path.join(C.DS_BASE_DIR, 'yolo_labels.json'), 'wt'), indent=2)

	C.NUM_CLASSES = len(dataset.labels())
	TRAINING_PARAMS['classes'] = C.NUM_CLASSES
	initial_wname = os.path.join(C.DS_BASE_DIR, C.INITIAL_WNAME) + '.pth'
	load_weights = os.path.exists(initial_wname)
	m = Darknet()
	if C.is_cuda():
		m = m.cuda(C.GPU_ID)

	if load_weights:
		print("Load pretrained weights from {}".format(initial_wname))
		m.load_state_dict(torch.load(initial_wname))


	# Initiate model
	model.apply(weights_init_normal)

	# If specified we start from checkpoint
	if opt.pretrained_weights:
		if opt.pretrained_weights.endswith(".pth"):
			model.load_state_dict(torch.load(opt.pretrained_weights))
		else:
			model.load_darknet_weights(opt.pretrained_weights)

	# Get dataloader
	dataset = ListDataset(train_path, augment=True, multiscale=opt.multiscale_training)
	dataloader = torch.utils.data.DataLoader(
		dataset,
		batch_size=opt.batch_size,
		shuffle=True,
		num_workers=opt.n_cpu,
		pin_memory=True,
		collate_fn=dataset.collate_fn,
	)

	optimizer = torch.optim.Adam(model.parameters())

	metrics = [
		"grid_size",
		"loss",
		"x",
		"y",
		"w",
		"h",
		"conf",
		"cls",
		"cls_acc",
		"recall50",
		"recall75",
		"precision",
		"conf_obj",
		"conf_noobj",
	]

	for epoch in range(opt.epochs):
		model.train()
		start_time = time.time()
		for batch_i, (_, imgs, targets) in enumerate(dataloader):
			batches_done = len(dataloader) * epoch + batch_i

			imgs = Variable(imgs.to(device))
			targets = Variable(targets.to(device), requires_grad=False)

			loss, outputs = model(imgs, targets)
			loss.backward()

			if batches_done % opt.gradient_accumulations:
				# Accumulates gradient before each step
				optimizer.step()
				optimizer.zero_grad()

			# ----------------
			#   Log progress
			# ----------------

			log_str = "\n---- [Epoch %d/%d, Batch %d/%d] ----\n" % (epoch, opt.epochs, batch_i, len(dataloader))

			metric_table = [["Metrics", *[f"YOLO Layer {i}" for i in range(len(model.yolo_layers))]]]

			# Log metrics at each YOLO layer
			for i, metric in enumerate(metrics):
				formats = {m: "%.6f" for m in metrics}
				formats["grid_size"] = "%2d"
				formats["cls_acc"] = "%.2f%%"
				row_metrics = [formats[metric] % yolo.metrics.get(metric, 0) for yolo in model.yolo_layers]
				metric_table += [[metric, *row_metrics]]

				# Tensorboard logging
				tensorboard_log = []
				for j, yolo in enumerate(model.yolo_layers):
					for name, metric in yolo.metrics.items():
						if name != "grid_size":
							tensorboard_log += [(f"{name}_{j+1}", metric)]
				tensorboard_log += [("loss", loss.item())]
				logger.list_of_scalars_summary(tensorboard_log, batches_done)

			log_str += AsciiTable(metric_table).table
			log_str += f"\nTotal loss {loss.item()}"

			# Determine approximate time left for epoch
			epoch_batches_left = len(dataloader) - (batch_i + 1)
			time_left = datetime.timedelta(seconds=epoch_batches_left * (time.time() - start_time) / (batch_i + 1))
			log_str += f"\n---- ETA {time_left}"

			print(log_str)

			model.seen += imgs.size(0)

		if epoch % opt.evaluation_interval == 0:
			print("\n---- Evaluating Model ----")
			# Evaluate the model on the validation set
			precision, recall, AP, f1, ap_class = evaluate(
				model,
				path=valid_path,
				iou_thres=0.5,
				conf_thres=0.5,
				nms_thres=0.5,
				img_size=opt.img_size,
				batch_size=8,
			)
			evaluation_metrics = [
				("val_precision", precision.mean()),
				("val_recall", recall.mean()),
				("val_mAP", AP.mean()),
				("val_f1", f1.mean()),
			]
			logger.list_of_scalars_summary(evaluation_metrics, epoch)

			# Print class APs and mAP
			ap_table = [["Index", "Class name", "AP"]]
			for i, c in enumerate(ap_class):
				ap_table += [[c, class_names[c], "%.5f" % AP[i]]]
			print(AsciiTable(ap_table).table)
			print(f"---- mAP {AP.mean()}")

		if epoch % opt.checkpoint_interval == 0:
			torch.save(model.state_dict(), f"checkpoints/yolov3_ckpt_%d.pth" % epoch)







	m.train(True)

	# Optimizer and learning rate
	optimizer = _get_optimizer(m)
	lr_scheduler = optim.lr_scheduler.StepLR(optimizer,
		step_size=TRAINING_PARAMS['lr']["decay_step"], gamma=TRAINING_PARAMS['lr']["decay_gamma"])

	# Set data parallel
	m = nn.DataParallel(m)


	# YOLO loss with 3 scales
	yolo_losses = []
	for i in range(3):
		yolo_losses.append(YOLOLoss(TRAINING_PARAMS["anchors"][i], C.NUM_CLASSES, (TRAINING_PARAMS["img_w"], TRAINING_PARAMS["img_h"])))

#	print(summary(m, (3, 416, 416), device='cpu'))

	dataloader = torch.utils.data.DataLoader(dataset, batch_size=C.BATCH_SIZE, shuffle=True, num_workers=32, pin_memory=True)

	with SummaryWriter(C.LOG_DIR, comment=f'LR_{C.LEARNING_RATE}_BS_{C.BATCH_SIZE}') as tf_writer:

		# Start the training loop
		print("Start training.")
		global_step = 0
		loss_f = 0
		for epoch in range(C.NUM_EPOCHS):
			loss_f = 0
			for step, samples in enumerate(dataloader):
				images, labels = samples["image"], samples["label"]
				start_time = time.time()
				global_step += 1

				# Forward and backward
				optimizer.zero_grad()
				outputs = m(images)
				losses_name = ["total_loss", "x", "y", "w", "h", "conf", "cls"]
				losses = []
				for _ in range(len(losses_name)):
					losses.append([])
				for i in range(3):
					_loss_item = yolo_losses[i](outputs[i], labels)
					for j, l in enumerate(_loss_item):
						losses[j].append(l)
				losses = [sum(l) for l in losses]
				loss = losses[0]
				loss.backward()
				optimizer.step()
				loss_f += loss.float()

				if step > 0 and step % 10 == 0:
					_loss = loss.item()
					duration = float(time.time() - start_time)
					example_per_second = C.BATCH_SIZE / duration
					lr = optimizer.param_groups[0]['lr']
					print(
						"epoch [%.3d] iter = %d loss = %.4f example/sec = %.3f lr = %.5f "%
						(epoch, step, _loss, example_per_second, lr)
					)
					tf_writer.add_scalar("lr", lr, global_step)
					tf_writer.add_scalar("example/sec", example_per_second, global_step)
					for i, name in enumerate(losses_name):
						value = _loss if i == 0 else losses[i]
						tf_writer.add_scalar(name, value, global_step)

			if loss_f < prev_loss:
				prev_loss = loss_f
				m.train(False)
				_save_checkpoint(m, loss_f, epoch)
				m.train(True)

			lr_scheduler.step()

		m.train(False)
		_save_checkpoint(m, loss_f, 'last')
		m.train(True)
	print('Finished')
