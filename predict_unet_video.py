import os
import glob
import json
from datasets import SegVideoDataset
from models import UNet
from data.labels import LABELS
from config import Config

C = Config('unet')
C.BATCH_SIZE = 4
C.PREDICT_VIDS_OUTPUT_DIR += '.unet'

import cv2
import torch
from utils.misc import create_or_clean_dirs
from utils.inference import predict_segmentation, predict_video_dir

def load_model():
	model = UNet(input_channels=C.NUM_INPUT_CHANNELS, output_channels=C.NUM_CLASSES).cuda(C.GPU_ID)
	print('###', os.path.join(C.DS_BASE_DIR, C.INITIAL_WNAME) + '.pth')
	model.load_state_dict(torch.load(os.path.join(C.DS_BASE_DIR, C.INITIAL_WNAME) + '.pth'))
	return model

if __name__ == "__main__":

	label_map = {}
	for l in LABELS:
		if 'segnet_val' in l:
			label_map[l['val']] = l['segnet_val']
		# +1 for background
		C.NUM_CLASSES = max(label_map.values()) + 1
	predict_video_dir(C, SegVideoDataset, load_model())
	print('Finished')
