import json
import os
import numpy as np
from config import Config

def dr(val):
	return round(val, 2)

for netname in ('segnet', 'unet'):

	C = Config(netname, ds_base_dir='data.truckload')

	for root, dirs, files in os.walk(C.PREDICT_VIDS_OUTPUT_DIR + '.' + netname):
		for fname in files:
			if fname.endswith('.json') and not fname.endswith('_processed.json'):
				print(fname)
				fname = os.path.join(root, fname)
				datalist = json.load(open(fname))
				if 'frames' in datalist:
					data = datalist
					datalist = data['frames']
				else:
					data = {'frames': datalist}
				n_truck_with_dirt = 0
				v_truck = []
				v_dirt = []
				for f in datalist:
					if 'truck' in f and f.get('dirt', 0) > 0:
						n_truck_with_dirt += 1
						v_truck.append(f['truck'])
						v_dirt.append(f['dirt'])
				data.update({
					'n_frames': len(datalist),
					'n_truck_with_dirt': n_truck_with_dirt,
					'total_truck': dr(np.sum(v_truck)) if v_truck else 0,
					'total_dirt': dr(np.sum(v_dirt)) if v_dirt else 0,
					'avg_dirt_in_truck_and_dirt': dr(np.mean(v_dirt)) if v_dirt else 0,
					'avg_truck_in_truck_and_dirt': dr(np.mean(v_truck)) if v_truck else 0,
					'stdev_dirt_in_truck_and_dirt': dr(np.std(v_dirt)) if v_dirt else 0,
					'stdev_truck_in_truck_and_dirt': dr(np.std(v_truck)) if v_truck else 0,
				})
				json.dump(data, open(os.path.splitext(fname)[0] + '_processed.json', 'w'), indent=2)
