TRAINING_PARAMS = \
{
	"model_params": {
		"backbone_name": "darknet_53"
	},
	"anchors": [[[37,35], [59,54], [86,88]], [[116,106], [123,176], [138,133]], [[164,165], [183,206], [219,280]]],
	"lr": {
		"backbone_lr": 0.001,
		"other_lr": 0.01,
		"freeze_backbone": False,   #  freeze backbone wegiths to finetune
		"decay_gamma": 0.6,
		"decay_step": 20,		   #  decay lr in every ? epochs
	},
	"optimizer": {
		"type": "sgd",
		"weight_decay": 4e-05,
	},
	"img_h": 416,
	"img_w": 416,
	"parallels": [0],						 #  config GPU device
	"confidence_threshold": 0.50,
}
