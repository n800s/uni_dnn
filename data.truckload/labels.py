LABELS = (
	{
		'class': 'dirt',
		'val': 1,
		'segnet_val': 1,
		'color': (0, 80, 80, 255),
	},
	{
		'class': 'coal',
		'val': 2,
		'segnet_val': 2,
		'color': (80, 80, 0, 255),
	},
	{
		'class': 'belaz',
		'val': 3,
		'segnet_val': 3,
		'segnet_class': 'truck',
		'color': (255, 0, 0, 255),
	},
	{
		'class': 'chinacar',
		'segnet_val': 3,
		'val': 4,
		'color': (128, 255, 0, 255),
	},
	{
		'class': 'watercar',
		'segnet_val': 3,
		'val': 5,
		'color': (0, 0, 255, 255),
	},
	{
		'class': 'tractor',
		'segnet_val': 3,
		'val': 6,
		'color': (0, 128, 128, 255),
	},
)

