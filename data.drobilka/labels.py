LABELS = (
	{
		'class': 'loader_dirt',
		'val': 1,
		'segnet_val': 1,
		'color': (165, 100, 3, 255),
	},
	{
		'class': 'car_dirt',
		'val': 2,
		'segnet_val': 2,
		'color': (4, 215, 208, 255),
	},
	{
		'class': 'dirt',
		'val': 3,
		'segnet_val': 3,
#		'yolo_val': 0,
		'color': (251, 235, 60, 255),
#		'order': -1,
	},
	{
		'class': 'loader_empty',
		'val': 4,
		'segnet_val': 4,
		'color': (30, 96, 250, 255),
	},
	{
		'class': 'car_empty',
		'val': 5,
		'segnet_val': 5,
		'color': (30, 196, 150, 255),
	},
)

